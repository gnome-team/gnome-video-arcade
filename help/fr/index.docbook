<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.5.6">
<!ENTITY manrevision "1.1">
<!ENTITY date "February 2008">
<!ENTITY app "GNOME Video Arcade">
]>
<article id="index" lang="fr">

  <articleinfo>

    <title>Manuel de <application>GNOME Video Arcade</application></title>

    <abstract role="description">
      <para>GNOME Video Arcade est une interface pour <application>Multiple Arcade Machine Emulator (MAME)</application>.</para>
    </abstract>

    <copyright lang="en">
      <year>2007</year>
      <year>2008</year>
      <holder>Matthew Barnes</holder>
    </copyright>

      <legalnotice id="legalnotice">
	<para lang="en">
	  Permission is granted to copy, distribute and/or modify this
	  document under the terms of the GNU Free Documentation
	  License (GFDL), Version 1.1 or any later version published
	  by the Free Software Foundation with no Invariant Sections,
	  no Front-Cover Texts, and no Back-Cover Texts.  You can find
	  a copy of the GFDL at this <ulink type="help" url="help:fdl">link</ulink> or in the file COPYING-DOCS
	  distributed with this manual.
         </para>
         <para>Ce manuel fait partie de la collection de manuels GNOME distribués selon les termes de la licence de documentation libre GNU. Si vous souhaitez distribuer ce manuel indépendamment de la collection, vous devez joindre un exemplaire de la licence au document, comme indiqué dans la section 6 de celle-ci.</para>

	<para>La plupart des noms utilisés par les entreprises pour distinguer leurs produits et services sont des marques déposées. Lorsque ces noms apparaissent dans la documentation GNOME et que les membres du projet de Documentation GNOME sont informés de l'existence de ces marques déposées, soit ces noms entiers, soit leur première lettre est en majuscule.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>



    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Matthew</firstname>
        <surname>Barnes</surname>
      </author>
    </authorgroup>

    <revhistory>
      <revision lang="en">
        <revnumber>GNOME Video Arcade Manual 1.1</revnumber>
        <date>2008-02-24</date>
      </revision>
    </revhistory>

    <releaseinfo>Ce manuel décrit la version 0.5.6 de GNOME Video Arcade</releaseinfo>

  
    <othercredit class="translator">
      <personname>
        <firstname>Julien Hardelin</firstname>
      </personname>
      <email>jhardlin@orange.fr</email>
    </othercredit>
    <copyright>
      
        <year>2012</year>
      
      <holder>Julien Hardelin</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Alexandre Franke</firstname>
      </personname>
      <email>alexandre.franke@gmail.com</email>
    </othercredit>
    <copyright>
      
        <year>2012</year>
      
      <holder>Alexandre Franke</holder>
    </copyright>
  </articleinfo>

  <section>
    <title>Introduction</title>
    <para>L'application <application>GNOME Video Arcade</application> vous permet de jouer aux jeux d'arcade à pièces classiques sur votre bureau GNOME grâce à <application>Multiple Arcade Machine Emulator (MAME)</application>.</para>
    <para><application>GNOME Video Arcade</application> apporte les fonctionnalités suivantes :</para>
    <itemizedlist>
      <listitem>
        <para>Jouer aux jeux d'arcade classiques sur votre ordinateur.</para>
      </listitem>
      <listitem>
        <para>Étiqueter vos jeux favoris pour les retrouver ensuite plus facilement.</para>
      </listitem>
      <listitem>
        <para>Lire les informations d'historique et les astuces concernant vos jeux favoris.</para>
      </listitem>
      <listitem>
        <para>Enregistrer les parties et les repasser.</para>
      </listitem>
      <listitem>
        <para>Rechercher.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section>
    <title>Premiers pas</title>
    <section>
      <title>Démarrage de GNOME Video Arcade</title>
      <para>Vous pouvez démarrer <application>GNOME Video Arcade</application> des manières suivantes :</para>
      <variablelist>
        <varlistentry>
          <term>Menu <guimenu>Applications</guimenu></term>
          <listitem>
            <para lang="en">
              Choose
              <menuchoice>
                <guisubmenu>Games</guisubmenu>
                <guimenuitem>Video Arcade</guimenuitem>
              </menuchoice>.
            </para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Ligne de commande</term>
          <listitem>
            <para>Pour démarrer <application>GNOME Video Arcade</application> depuis la ligne de commande, saisissez la commande suivante et appuyez sur <keycap>Entrée</keycap> :</para>
            <para><command>gnome-video-arcade</command></para>
            <tip>
              <para>Pour afficher les autres options de la ligne de commande, saisissez <command>gnome-video-arcade --help</command> et appuyez sur <keycap>Entrée</keycap>.</para>
            </tip>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
    <section>
      <title>Au démarrage de GNOME Video Arcade</title>
      <para>Lors de son premier démarrage, <application>GNOME Video Arcade</application> construit une base de données concernant les jeux et analyse les fichiers ROM pour déterminer les jeux disponibles. Ce processus peut durer plusieurs minutes, pendant lesquelles la fenêtre suivante est affichée :</para>
      <figure>
        <title>Construction de la base de données des jeux par GNOME Video Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_building_database.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Présente la fenêtre principale de GNOME Video Arcade pendant la construction de la base de données. La plupart des éléments sont grisés et une barre de progression est visible en bas.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>La fenêtre change alors pour ressembler à ceci :</para>
      <figure>
        <title>Fenêtre de démarrage de GNOME Video Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_main_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Affiche la fenêtre principale de GNOME Video Arcade avec un échantillon de jeux. Contient une barre de menus, une liste de jeux, des boutons d'affichage, un bouton pour démarrer le jeu choisi, un bouton de propriétés et une barre d'état.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Vous apercevez une liste jeux dépendant des images de ROM installées sur votre système.</para>
      <tip>
        <para>Si vous savez quelles sont les images de ROM installées sur votre système, mais que GNOME Video Arcade ne dresse la liste d'aucun jeu, c'est peut-être que <application>MAME</application> est mal configuré ou qu'aucune des images de ROM installées n'est jouable. Reportez-vous au chapitre de détection des pannes pour de plus amples informations.</para>
      </tip>
      <note>
        <para>Occasionnellement, la base de données peut être reconstruite quand vous démarrez <application>GNOME Video Arcade</application>. La plupart du temps, le programme saute cette étape et passe directement à la fenêtre de la Figure 2.</para>
      </note>
      <para>La fenêtre de <application>GNOME Video Arcade</application> contient les éléments suivants :</para>
      <variablelist>
        <varlistentry>
          <term>Barre de menus</term>
          <listitem>
            <para>Les menus de la barre de menus renferment toutes les commandes nécessaires à l'utilisation de <application>GNOME Video Arcade</application>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Liste de jeux</term>
          <listitem>
            <para>La liste de jeux affiche les jeux que vous pouvez choisir.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Boutons d'affichage</term>
          <listitem>
            <para>Les boutons d'affichage contrôlent quels jeux sont affichés dans la liste des jeux. Vous avez le choix entre afficher tous les jeux disponibles et une liste de jeux favoris, ou les résultats d'une recherche préalable.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Bouton <guibutton>Démarrer le jeu</guibutton></term>
          <listitem>
            <para>Le bouton <guibutton>Démarrer le jeu</guibutton> lance le jeu qui est en surbrillance dans la liste des jeux.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Bouton <guibutton>Propriétés</guibutton></term>
          <listitem>
            <para>Le bouton <guibutton>Propriétés</guibutton> affiche des informations complémentaires sur le jeu en surbrillance dans la liste des jeux.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Barre d'état</term>
          <listitem>
            <para>La barre d'état affiche la version de <application>MAME</application> utilisée pour jouer aux jeux et une information contextuelle sur les éléments du menu. Elle affiche également une barre de progression lors de la création de la base de jeux. </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
  </section>

  <section>
    <title>Utilisation </title>
    <section>
      <title>Démarrer un jeu</title>
      <para lang="en">
        Choose a game from the game list and click on it to highlight it.
        Then click the <guibutton>Start Game</guibutton> button or choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>S</keycap>
            </keycombo>
          </shortcut>
          <guimenu>Game</guimenu>
          <guimenuitem>Start</guimenuitem>
        </menuchoice>.
      </para>
      <tip>
        <para>Vous pouvez aussi démarrer un jeu par un double-clic sur son nom dans la liste des jeux. </para>
      </tip>
    </section>
    <section>
      <title>Afficher les propriétés d'un jeu</title>
      <para lang="en">
        Choose a game from the game list and click on it to highlight it.
        Then click the <guibutton>Properties</guibutton> button or choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>P</keycap>
            </keycombo>
          </shortcut>
          <guimenu>View</guimenu>
          <guimenuitem>Properties</guimenuitem>
        </menuchoice>.
      </para>
      <para>La boîte de dialogue <guilabel>Propriétés</guilabel> affiche les informations concernant le jeu en surbrillance, dont un court historique et des astuces pour vous aider à améliorer vos scores, des images de la borne d'arcade, des captures d'écrans du jeu en cours d'exécution et certaines informations techniques sur l'émulation <application>MAME</application> du jeu. </para>
      <para>Pour fermer la boîte de dialogue <guilabel>Propriétés</guilabel>, cliquez sur le bouton <guibutton>Fermer</guibutton>. </para>
      <note>
        <para>Certaines de ces fonctionnalités ne sont pas encore achevées et seront disponibles dans une version future de <application>GNOME Video Arcade</application>.</para>
      </note>
    </section>
    <section>
      <title>Pour étiqueter un jeu comme favori</title>
      <para lang="en">
        To tag a game as a favorite, choose a game from the game list and
        click on it to highlight it.  Then choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>+</keycap>
            </keycombo>
          </shortcut>
          <guimenu>Game</guimenu>
          <guimenuitem>Add to Favorites</guimenuitem>
        </menuchoice>.
        The heart icon for the highlighted game will light up to indicate
        that the game has been added to your list of favorites.
      </para>
      <para lang="en">
        If a game is no longer your favorite, click on its name in the game
        list to highlight it.  Then choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>-</keycap>
            </keycombo>
          </shortcut>
          <guimenu>Game</guimenu>
          <guimenuitem>Remove from Favorites</guimenuitem>
        </menuchoice>.
        The heart icon for the highlighted game will turn gray to indicate
        that the game has been removed from your list of favorites.
      </para>
      <tip>
        <para>Vous pouvez aussi cliquer directement sur une icône cœur dans la liste des jeux pour étiqueter le jeu comme favori ou non.</para>
      </tip>
    </section>
    <section>
      <title>Modifier la liste des jeux affichés</title>
      <para lang="en">
        To show all available games in the game list, click the
        <guibutton>Available Games</guibutton> view button or choose
        <menuchoice>
          <guimenu>View</guimenu>
          <guimenuitem>Available Games</guimenuitem>
        </menuchoice>.
      </para>
      <para lang="en">
        To show only your favorite games in the game list, click the
        <guibutton>Favorite Games</guibutton> view button or choose
        <menuchoice>
          <guimenu>View</guimenu>
          <guimenuitem>Favorite Games</guimenuitem>
        </menuchoice>.
      </para>
      <para lang="en">
        To show your previous search results in the game list, click the
        <guibutton>Search Results</guibutton> view button or choose
        <menuchoice>
          <guimenu>View</guimenu>
          <guimenuitem>Search Results</guimenuitem>
        </menuchoice>.
      </para>
    </section>
    <section>
      <title>Pour chercher des jeux</title>
      <para lang="en">
        Choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>F</keycap>
            </keycombo>
          </shortcut>
          <guimenu>Edit</guimenu>
          <guimenuitem>Search...</guimenuitem>
        </menuchoice>
        to display the following dialog:
      </para>
      <figure>
        <title>Dialogue Recherche de GNOME Video Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_search_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Affiche la boîte de dialogue Recherche de GNOME Video Arcade. Contient une boîte pour la saisie de texte, un bouton de recherche et un bouton de fermeture.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Saisissez vos critères de recherche dans la boîte de saisie. Recherchez par titre de jeu, par société de production de jeux, par année de distribution de jeu, par nom de la collection de ROM de jeu ou par pilote <application>MAME</application>.</para>
      <para>Cliquez sur le bouton <guibutton>Rechercher</guibutton> pour lancer la recherche. La boîte de dialogue <guilabel>Rechercher</guilabel> se ferme et les résultats de la recherche sont affichés dans la liste des jeux.</para>
      <para>Cliquez sur le bouton <guibutton>Fermer</guibutton> pour fermer la boîte de dialogue <guilabel>Rechercher</guilabel> sans effectuer de recherche.</para>
    </section>
    <section>
      <title>Pour sauvegarder une partie</title>
      <para lang="en">
        Choose a game from the game list and click on it to highlight it.
        Then choose
        <menuchoice>
          <shortcut>
            <keycombo>
              <keycap>Ctrl</keycap>
              <keycap>R</keycap>
            </keycombo>
          </shortcut>
          <guimenu>Game</guimenu>
          <guimenuitem>Record</guimenuitem>
        </menuchoice>.
      </para>
      <para>Après être sorti de <application>MAME</application>, votre nouvel enregistrement sera mis en surbrillance dans la fenêtre suivante :</para>
      <figure>
        <title>Fenêtre « Parties sauvegardées » de GNOME Video Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_recorded_games.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Présente la fenêtre « Parties enregistrées » de GNOME Video Arcade. Contient une liste des parties enregistrées, un bouton de reprise du jeu, un bouton de suppression et un bouton de fermeture.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Chaque entrée donne le titre du jeu enregistré, ainsi que la date et l'heure du début de l'enregistrement. Cliquez sur le titre du jeu pour adapter le commentaire. Vous pouvez ajouter quelque information au commentaire, comme le score final ou le niveau atteint.</para>
    </section>
    <section>
      <title>Pour revoir une partie enregistrée</title>
      <para lang="en">
        Choose
        <menuchoice>
          <guimenu>Game</guimenu>
          <guimenuitem>Play Back...</guimenuitem>
        </menuchoice>
        to display the <guilabel>Recorded Games</guilabel> window.
        Choose a game recording to play back and click on it to highlight it.
        Then click the <guibutton>Play Back</guibutton> button.
      </para>
    </section>
  </section>

  <section>
    <title>Préférences</title>
    <para lang="en">
      To modify the preferences of <application>GNOME Video Arcade</application>, choose
      <menuchoice>
        <guimenu>Edit</guimenu>
        <guimenuitem>Preferences</guimenuitem>
      </menuchoice>.
    </para>
    <para>Le dialogue <guilabel>Préférences</guilabel> contient les éléments suivants :</para>
    <section>
      <title>Général</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_general_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Affiche l'onglet « Général » de la fenêtre « Préférences ».</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <variablelist>
        <varlistentry>
          <term lang="en">
            <guilabel>
              Start games in fullscreen mode
            </guilabel>
          </term>
          <listitem>
            <para>Sélectionnez cette option pour démarrer les jeux en plein écran. Sinon, les jeux démarrent dans une fenêtre.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term lang="en">
            <guilabel>
              Restore previous state when starting a game
            </guilabel>
          </term>
          <listitem>
            <para>Sélectionnez cette option pour reprendre là où vous en étiez quand vous avez quitté <application>MAME</application>. Si cette option n'est pas sélectionnée, l'émulateur amorce la machine d'arcade chaque fois qu'un jeu est démarré.</para>
            <note>
              <para>La restauration d'état ne prend pas effet avant le deuxième démarrage du jeu quand cette option est activée. Elle ne s'applique pas à l'enregistrement et à l'action de rejouer.</para>
            </note>
            <warning>
              <para>Tous les jeux n'acceptent pas cette fonctionnalité.</para>
            </warning>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term lang="en">
            <guilabel>
              Show alternate versions of original games
            </guilabel>
          </term>
          <listitem>
            <para>Sélectionnez cette option pour afficher les versions alternatives des jeux originaux dans la liste de jeux de la fenêtre principale.</para>
          </listitem>
        </varlistentry>
      </variablelist>
      <note>
        <title>Que sont ces versions alternatives ?</title>
          <para>Selon la <ulink url="http://mamedev.org/devwiki/index.php/FAQ:General_Games"> FAQ <application>MAME</application></ulink> :</para>
          <blockquote>
            <para>Les jeux étaient souvent produits sous licence par d'autres sociétés pour diverses raisons : la plus commune était que les fabricants désiraient vendre le jeu dans une région ou pays où ils ne possédaient pas de réseau de distribution qui leur soit propre. Au début des années 80, Namco n'avait de réseau de distribution aux USA et ses jeux étaient souvent produits sous licence par Atari, Bally-Midway et d'autres. Inversement,  Atari accordait licence pour ses jeux à des sociétés japonaises (habituellement Namco), afin de les vendre au Japon.</para>
            <para>Parfois une collection de ROM avec une date de copyright différente est trouvée, ou une collection piratée, ou une autre version alternative. Si le jeu <quote>parent</quote> a déjà été émulé dans <application>MAME</application>, ces versions alternatives sont habituellement faciles à ajouter. Dans certains cas, les versions alternatives sont légèrement différentes : les niveaux de jeu sont dans un ordre différent, le jeu est plus difficile ou est plus rapide, etc.</para>
            <para>Dans quelques cas, les ROM ont été trafiquées pour fonctionner sur des matériels pour lesquels elle n'étaient pas prévues. Par exemple, quand <productname>Pac-Man</productname> était populaire, certains opérateurs d'arcade trouvèrent le moyen de copier les ROM de <productname>Pac-Man</productname> et de les transformer pour fonctionner sur les machines <productname>Scramble</productname> et ainsi ne pas avoir à acheter un <productname>Pac-Man</productname> supplémentaire. Puisque ces versions trafiquées sont de véritables chefs-d'œuvre dus à leur seul talent, les développeurs de <application>MAME</application> les ont incluses.</para>
          </blockquote>
      </note>
    </section>
    <section>
      <title>Colonnes</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_column_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Présente l'onglet des colonnes de la fenêtre Préférences</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <para>L'onglet <guilabel>Colonnes</guilabel> vous permet de configurer la liste de jeux dans la fenêtre principale pour afficher des informations complémentaires sur les jeux disponibles. Les noms de colonnes sont disposés en liste dans l'ordre où ils apparaissent dans la liste des jeux. Réordonnez les colonnes et affichez des colonnes supplémentaires grâce aux boutons <guibutton>Déplacer vers le haut</guibutton>, <guibutton>déplacer vers le bas</guibutton>, <guibutton>Afficher</guibutton> et <guibutton>Masquer</guibutton> à droite de la liste des colonnes.</para>
    </section>
  </section>

  <section>
    <title>Dépannage</title>
    <anchor id="troubleshooting"/>
    <section>
      <title>Aucun jeu dans la liste de la fenêtre principale</title>
        <section>
          <title>Des images de ROM sont-elles installées ?</title>
          <para>Pour des raisons légales, les jeux ne sont pas livrés avec <application>GNOME Video Arcade</application> or <application>MAME</application>. La <ulink url="http://mamedev.org/devwiki/index.php/FAQ:Setting_Up"> FAQ <application>MAME</application></ulink> fournit quelques suggestions pour obtenir des images de ROM :</para>
          <blockquote>
            <para>Il existe plusieurs façons d'obtenir des images de ROM à utiliser dans <application>MAME</application> :</para>
            <itemizedlist>
              <listitem>
                <para>Un petit nombre d'images de ROM sont maintenant disponibles avec l'autorisation du détenteur des droits d'auteur sur la <ulink url="http://mamedev.org/roms/">page des ROM mamedev.org </ulink>.</para>
              </listitem>
              <listitem>
                <para>Le <ulink url="http://www.hanaho.com/Products/HotRodJoystick.php">panneau de commande du <productname>HotRod Joystick</productname> </ulink> de <ulink url="http://www.hanaho.com/">Hanaho Games, Inc.</ulink> est livré avec un CD incluant <application>MAME</application> et une sélection de ROMs Capcom. Hanaho également vend la <ulink url="http://www.hanaho.com/Products/ArcadePC.php"><productname>borne ArcadePC</productname></ulink> avec une autre sélection de ROMs Capcom.</para>
              </listitem>
              <listitem>
                <para>Vous pouvez acheter vos propres cartes électroniques. Achetez un lecteur de ROM et servez-vous en pour copier le contenu des puces de la ROM dans des fichiers pour votre usage personnel.</para>
              </listitem>
            </itemizedlist>
            <para>Vérifiez toujours que le détenteur des droits d'auteur des jeux d'arcade vend les ROM légalement (comme c'est le cas avec Capcom). De cette façon, vous apporterez votre soutien aux sociétés qui soutiennent l'émulation.</para>
          </blockquote>
        </section>
        <section>
          <title><application>MAME</application> peut-il trouver les images de ROM installées ?</title>
          <para><application>GNOME Video Arcade</application> s'appuie sur  <application>MAME</application> pour trouver les images de ROM installées. Pour tester où <application>MAME</application> est configuré pour chercher les images de ROM, saisissez la commande suivante dans une ligne de commande et appuyez sur <keycap>Entrée</keycap> :</para>
          <para lang="en">
            <command>gnome-video-arcade --inspect rompath</command>
          </para>
          <para>La commande devrait afficher un nom de répertoire comme <computeroutput>/usr/share/mame/roms</computeroutput>. Déplacez les images de ROM  vers ce répertoire et redémarrez <application>GNOME Video Arcade</application>.</para>
          <para>Si la commande n'affiche pas de nom de répertoire, cela peut signifier que <application>MAME</application> n'est pas correctement configuré sur votre système. Si vous avez obtenu <application>MAME</application> depuis une distribution <productname>GNU/Linux</productname>, contactez les mainteneurs de cette distribution pour des instructions supplémentaires.</para>
        </section>
        <section>
          <title>L'affichage des jeux disponibles est-il sélectionné ?</title>
          <para>Si aucun jeu n'a été étiqueté comme favori, l'affichage de <guilabel>Jeux disponibles</guilabel> est vide. De même, si aucune recherche n'a été effectuée, l'affichage <guilabel>Résultats de recherche</guilabel> est vide lui aussi. Passez à l'affichage <guilabel>Jeux disponibles</guilabel> pour vous assurer que tous les jeux disponibles figurent dans la liste.</para>
        </section>
    </section>
  </section>

</article>
