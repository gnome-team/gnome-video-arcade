<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.5.6">
<!ENTITY manrevision "1.1">
<!ENTITY date "February 2008">
<!ENTITY app "GNOME Video Arcade">
]>
<article id="index" lang="el">

  <articleinfo>

    <title>Εγχειρίδιο <application>GNOME Video Arcade</application></title>

    <abstract role="description">
      <para>Το GNOME Video Arcade είναι μια διεπαφή χρήστη για τον <application>πολλαπλό προσομοιωτή μηχανής Arcade (MAME)</application>.</para>
    </abstract>

    <copyright><year>2007</year> <year>2008</year> <holder>Matthew Barnes</holder></copyright>

      <legalnotice id="legalnotice">
	<para>Δίνεται άδεια για αντιγραφή, διανομή και/ή τροποποίηση του εγγράφου υπό τους όρους της GNU Free Documentation License (GFDL), Έκδοση 1.1 ή μεταγενέστερη εκδιδόμενη από το Ίδρυμα Ελεύθερου Λογισμικού χωρίς Σταθερά Εδάφια, χωρίς Εξώφυλλα Κειμένου, και χωρίς Οπισθόφυλλα Κειμένου. Μπορείτε να βρείτε ένα αντίγραφο της GFDL σε αυτόν τον <ulink type="help" url="help:fdl">σύνδεσμο</ulink> ή στο αρχείο COPYING-DOCS που διανεμήθηκε με αυτόν τον οδηγό.</para>
         <para>Αυτή η τεκμηρίωση είναι μέρος της συλλογής τεκμηρίωσης του GNOME όπως διανέμεται υπό τους όρους της GFDL. Εάν επιθυμείτε να διανείμετε αυτή την τεκμηρίωση ξεχωριστά από την συλλογή, μπορείτε να το κάνετε εάν η τεκμηρίωση συνοδεύεται από αντίγραφο της άδειας (GFDL) όπως περιγράφεται στον τομέα 6 της άδειας.</para>

	<para>Πολλά από τα ονόματα που χρησιμοποιούνται από επιχειρήσεις για να διακρίνουν τα προϊόντα τους και τις υπηρεσίες αξιώνονται ως εμπορικά σήματα. Όπου εμφανίζονται αυτά τα ονόματα σε κάποια τεκμηρίωση του GNOME και τα μέλη του έργου τεκμηρίωσης του GNOME γνωρίζουν για αυτά τα εμπορικά σήματα, τότε τα ονόματα είναι με κεφαλαία γράμματα ή με αρχικά κεφαλαία γράμματα.</para>

	<para>TΟ ΕΓΓΡΑΦΟ ΚΑΙ ΟΙ ΤΡΟΠΟΙΗΜΕΝΕΣ ΕΚΔΟΣΕΙΣ ΤΟΥ ΠΑΡΕΧΟΝΤΑΙ ΥΠΟ ΤΟΥΣ ΟΡΟΥΣ ΤΗΣ ΕΛΕΥΘΕΡΗΣ ΑΔΕΙΑΣ ΤΕΚΜΗΡΙΩΣΗΣ GNU ΜΕ ΤΙΣ ΕΠΙΠΛΕΟΝ ΓΝΩΣΤΟΠΟΙΗΣΕΙΣ: <orderedlist>
		<listitem>
		  <para>Η ΤΕΚΜΗΡΙΩΣΗ ΑΥΤΗ ΠΑΡΕΧΕΤΑΙ "ΩΣ ΕΧΕΙ", ΧΩΡΙΣ ΚΑΜΙΑ ΕΓΓΥΗΣΗ ΚΑΝΕΝΟΣ ΕΙΔΟΥΣ, ΕΚΦΡΑΣΜΕΝΗ 'Η ΝΟΟΥΜΕΝΗ, ΠΕΡΙΛΑΜΒΑΝΟΜΕΝΩΝ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟ, ΕΓΓΥΗΣΕΩΝ ΟΤΙ ΤΟ ΕΓΓΡΑΦΟ 'Η ΠΑΡΑΛΛΑΓΕΣ ΤΟΥ ΕΓΓΡΑΦΟΥ ΕΙΝΑΙ ΕΛΕΥΘΕΡΑ ΚΑΘΕ ΕΛΑΤΤΩΜΑΤΟΣ, ΕΜΠΟΡΕΥΣΙΜΑ, ΚΑΤΑΛΛΗΛΑ ΓΙΑ ΕΝΑ ΣΥΓΚΕΚΡΙΜΕΝΟ ΣΚΟΠΟ, Η ΜΗ-ΠΑΡΑΒΑΙΝΟΝΤΑ. Η ΕΥΘΥΝΗ ΓΙΑ ΤΗΝ ΠΟΙΟΤΗΤΑ, ΑΚΡΙΒΕΙΑ, ΚΑΙ ΑΠΟΔΟΣΗ ΤΟΥ ΕΓΓΡΑΦΟΥ ΕΙΝΑΙ ΑΠΟΚΛΕΙΣΤΙΚΑ ΔΙΚΗ ΣΑΣ. ΑΝ ΟΠΟΙΟΔΗΠΟΤΕ ΕΓΓΡΑΦΟ Η ΠΑΡΑΛΛΑΓΗ ΤΟΥΑΠΟΔΕΙΚΤΟΥΝ ΕΛΑΤΤΩΜΑΤΙΚΑ ΜΕ ΟΠΟΙΟΔΗΠΟΤΕ ΤΡΟΠΟ, ΕΣΕΙΣ (ΚΑΙ ΟΧΙ Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ 'Η ΟΠΟΙΟΣΔΗΠΟΤΕ ΣΥΜΒΑΛΛΟΝΤΑΣ) ΑΝΑΛΑΜΒΑΝΕΤΕ ΤΟ ΚΟΣΤΟΣ ΟΠΟΙΑΣΔΗΠΟΤΕ ΑΝΑΓΚΑΙΑΣ ΥΠΗΡΕΣΙΑΣ, ΕΠΙΣΚΕΥΗΣ 'Η ΔΙΟΡΘΩΣΗΣ. Η ΑΠΑΛΛΑΓΗ ΕΥΘΥΝΗΣ ΠΟΥ ΣΥΝΟΔΕΥΕΙ ΑΥΤΗ ΤΗΝ ΤΕΚΜΗΡΙΩΣΗ ΕΙΝΑΙ ΑΝΑΠΟΣΠΑΣΤΟ ΚΟΜΜΑΤΙ ΑΥΤΗΣ. ΚΑΜΙΑ ΧΡΗΣΗ ΤΗΣ ΤΕΚΜΗΡΙΩΣΗΣ Ή ΠΑΡΑΛΛΑΓΩΝ ΑΥΤΗΣ ΔΕΝ ΕΠΙΤΡΕΠΕΤΑΙ ΠΑΡΑ ΜΟΝΟ ΕΑΝ ΣΥΝΟΔΕΥΕΤΑΙ ΑΠΟ ΤΗΝ ΑΠΑΛΛΑΓΗ ΕΥΘΥΝΗΣ. ΚΑΙ</para>
		</listitem>
		<listitem>
		  <para>ΚΑΙ ΣΕ ΚΑΜΙΑ ΠΕΡΙΠΤΩΣΗ ΚΑΙ ΣΕ ΚΑΜΙΑ ΝΟΜΟΛΟΓΙΑ ΑΣΤΙΚΟΥ ΑΔΙΚΗΜΑΤΟΣ (ΠΕΡΙΛΑΜΒΑΝΟΜΕΝΗΣ ΕΚΕΙΝΟΥ ΤΗΣ ΑΜΕΛΕΙΑΣ), ΣΥΜΒΟΛΑΙΟΥ Ή ΑΛΛΟΥ, Ο ΣΥΝΤΑΚΤΗΣ, Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ, ΟΠΟΙΟΣΔΗΠΟΤΕ ΣΥΝΕΡΓΑΤΗΣ Ή ΟΠΟΙΟΣΔΗΠΟΤΕ ΔΙΑΝΟΜΕΑΣ ΤΟΥ ΕΓΓΡΑΦΟΥ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΤΟΥ ΕΓΓΡΑΦΟΥ, Ή ΟΠΟΙΣΔΗΠΟΤΕ ΠΡΟΜΗΘΕΥΤΗΣ ΟΠΟΙΟΥΔΗΠΟΤΕ ΕΚ ΤΩΝ ΑΝΩΤΕΡΩ ΠΡΟΣΩΠΩΝ ΕΙΝΑΙ ΥΠΕΥΘΥΝΟΣ ΕΝΑΝΤΙ ΟΙΟΥΔΗΠΟΤΕ ΠΡΟΣΩΠΟΥ ΓΙΑ ΟΠΟΙΑΔΗΠΟΤΕ ΑΜΕΣΗ, ΕΜΜΕΣΗ, ΙΔΙΑΙΤΗΕΡΗ, ΑΤΥΧΗ Ή ΣΥΝΕΠΑΓΟΜΕΝΗ ΒΛΑΒΗ ΟΙΟΥΔΗΠΟΤΕ ΧΑΡΑΚΤΗΡΑ ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΩΝ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟΥΣ, ΒΛΑΒΕΣ ΕΞΑΙΤΙΑΣ ΑΠΩΛΕΙΑΣ ΠΕΛΑΤΕΙΑΣ, ΣΤΑΣΗ ΕΡΓΑΣΙΑΣ, ΑΣΤΟΧΙΑ Ή ΚΑΚΗ ΛΕΙΤΟΥΡΓΙΑ ΥΠΟΛΟΓΙΣΤΗ, Ή ΟΠΟΙΑΔΗΠΟΤΕ ΚΑΙ ΚΑΘΕ ΑΛΛΗ ΒΛΑΒΗ Ή ΑΠΩΛΕΙΑ ΠΟΥ ΘΑ ΕΓΕΙΡΕΙ Ή ΣΧΕΤΙΖΕΤΑΙ ΜΕ ΤΗΝ ΧΡΗΣΗ ΤΟΥ ΕΓΓΡΑΦΟΥ ΚΑΙ ΟΠΟΙΑΣΔΗΠΟΤΕ ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΤΟΥ ΕΓΓΡΑΦΟΥ, ΑΚΟΜΗ ΚΑΙ ΑΝ ΚΑΠΟΙΟ ΑΠΟ ΑΥΤΑ ΤΑ ΠΡΟΣΩΠΑ ΕΙΝΑΙ ΕΝΗΜΕΡΟ ΓΙΑ ΤΗΝ ΠΙΘΑΝΟΤΗΤΑ ΠΡΟΚΛΗΣΗΣ ΤΕΤΟΙΩΝ ΒΛΑΒΩΝ.</para>
		</listitem>
	  </orderedlist></para>
  </legalnotice>



    <authorgroup>
      <author role="maintainer"><firstname>Matthew</firstname> <surname>Barnes</surname></author>
    </authorgroup>

    <revhistory>
      <revision><revnumber>Εγχειρίδιο GNOME Video Arcade 1.1</revnumber> <date>24-02-2008</date></revision>
    </revhistory>

    <releaseinfo>Αυτό το εγχειρίδιο περιγράφει την έκδοση 0.5.6 του GNOME Video Arcade</releaseinfo>

  
    <othercredit class="translator">
      <personname>
        <firstname> Δημήτρης Σπίγγος</firstname>
      </personname>
      <email>dmtrs32@gmail.com</email>
    </othercredit>
    <copyright>
      
        <year>2014</year>
      
      <holder> Δημήτρης Σπίγγος</holder>
    </copyright>
  </articleinfo>

  <section>
    <title>Εισαγωγή</title>
    <para>Η εφαρμογή <application>GNOME Video Arcade</application> σας επιτρέπει να παίξετε κλασικά παιχνίδια arcade με κέρματα στην επιφάνεια εργασίας του GNOME χρησιμοποιώντας τον <application>πολλαπλό προσομοιωτή μηχανής Arcade (MAME)</application>.</para>
    <para>Το <application>GNOME Video Arcade</application> παρέχει τα παρακάτω χαρακτηριστικά:</para>
    <itemizedlist>
      <listitem>
        <para>Να παίζετε κλασικά παιχνίδια arcade στην επιφάνεια εργασίας σας.</para>
      </listitem>
      <listitem>
        <para>Να βάλετε ετικέτα στα αγαπημένα παιχνίδια για να τα βρίσκετε εύκολα αργότερα.</para>
      </listitem>
      <listitem>
        <para>Να διαβάζετε ιστορικές πληροφορίες και συμβουλές για τα αγαπημένα σας παιχνίδια.</para>
      </listitem>
      <listitem>
        <para>Να γράφετε το παιχνίδι και να το αναπαράγετε.</para>
      </listitem>
      <listitem>
        <para>Αναζήτηση.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section>
    <title>Ξεκινώντας</title>
    <section>
      <title>Έναρξη του GNOME βίντεο arcade</title>
      <para>Μπορείτε να αρχίσετε το <application>GNOME Video Arcade</application> με τους παρακάτω τρόπους:</para>
      <variablelist>
        <varlistentry>
          <term>Μενού <guimenu>Εφαρμογές</guimenu></term>
          <listitem>
            <para>Επιλέξτε <menuchoice> <guisubmenu>Παιχνίδια</guisubmenu> <guimenuitem>Βίντεο Arcade</guimenuitem> </menuchoice>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Γραμμή εντολών</term>
          <listitem>
            <para>Για να ξεκινήσετε το <application>GNOME Video Arcade</application> από τη γραμμή εντολών, πληκτρολογήστε την παρακάτω εντολή, έπειτα πατήστε το πλήκτρο <keycap>επιστροφή</keycap>:</para>
            <para><command>gnome-video-arcade</command></para>
            <tip>
              <para>Για να προβάλλετε άλλες επιλογές της γραμμής εντολών που είναι διαθέσιμες, πληκτρολογήστε <command>gnome-video-arcade --help</command>, έπειτα πατήστε το πλήκτρο <keycap>επιστροφή</keycap>.</para>
            </tip>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
    <section>
      <title>Όταν αρχίζετε το GNOME Βίντεο Arcade</title>
      <para>Την πρώτη φορά που αρχίζει το <application>GNOME Βίντεο Arcade</application>, θα δομήσει μια βάση δεδομένων των πληροφοριών του παιχνιδιού και θα αναλύσει τα διαθέσιμα αρχεία ROM για να προσδιορίσει ποια παιχνίδια μπορούν να παιχτούν. Αυτή η διεργασία μπορεί να πάρει αρκετά λεπτά για να ολοκληρωθεί και κατά τη διάρκεια της εμφανίζεται το παρακάτω παράθυρο:</para>
      <figure>
        <title>GNOME Βίντεο Arcade Δόμηση της βάσης δεδομένων του παιχνιδιού</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_building_database.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Εμφανίζει το κυρίως παράθυρο του GNOME Βίντεο Arcade, ενώ δημιουργείται η βάση δεδομένων. Τα περισσότερα στοιχεία είναι αχνά και μια γραμμή προόδου εμφανίζεται στο τέλος.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Το παράθυρο τότε αλλάζει για να φαίνεται όπως αυτό:</para>
      <figure>
        <title>Το αρχικό παράθυρο του GNOME Βίντεο Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_main_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Εμφανίζει το κυρίως παράθυρο του GNOME Βίντεο Arcade με έναν κατάλογο δειγμάτων παιχνιδιών. Περιέχει τη γραμμή μενού, τον κατάλογο παιχνιδιών, τα πλήκτρα προβολής, το πλήκτρο έναρξης παιχνιδιού, το πλήκτρο ιδιοτήτων και τη γραμμή κατάστασης.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Μπορεί να δείτε έναν διαφορετικό κατάλογο παιχνιδιών, ανάλογα με το ποιες εικόνες ROM είναι εγκατεστημένες στο σύστημά σας.</para>
      <tip>
        <para>Αν ξέρετε ότι εικόνες ROM είναι εγκατεστημένες στο σύστημά σας, αλλά το <application>GNOME Βίντεο Arcade</application> δεν εμφανίζει κανένα παιχνίδι, μπορεί να συμβαίνει το <application>MAME</application> να είναι απορυθμισμένο ή καμία από τις εγκατεστημένες εικόνες ROM να μην παίζει. Δείτε την ενότητα αντιμετώπισης προβλημάτων για περισσότερες πληροφορίες.</para>
      </tip>
      <note>
        <para>Ενίοτε, η βάση δεδομένων μπορεί να ξαναδημιουργείται όταν το <application>GNOME Βίντεο Arcade</application> αρχίζει. Τις περισσότερες φορές θα παραλείπει αυτή τη διεργασία και θα πηγαίνει απ' ευθείας στο παράθυρο στο σχήμα 2.</para>
      </note>
      <para>Το παράθυρο <application>GNOME Βίντεο Arcade</application> περιέχει τα παρακάτω στοιχεία:</para>
      <variablelist>
        <varlistentry>
          <term>Γραμμή μενού</term>
          <listitem>
            <para>Τα μενού στη γραμμή μενού περιέχουν όλες τις απαραίτητες εντολές που χρησιμοποιούνται στο <application>GNOME Βίντεο Arcade</application>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Κατάλογος παιχνιδιών</term>
          <listitem>
            <para>Ο κατάλογος παιχνιδιών εμφανίζει έναν κατάλογο παιχνιδιών για να διαλέξετε.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Πλήκτρα προβολής</term>
          <listitem>
            <para>Ο έλεγχος πλήκτρων προβολής για το ποια παιχνίδια εμφανίζονται στον κατάλογο παιχνιδιών. Επιλέξτε μεταξύ προβολής όλων των διαθέσιμων παιχνιδιών, ενός καταλόγου αγαπημένων παιχνιδιών, ή των αποτελεσμάτων προηγούμενης αναζήτησης.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Πλήκτρο <guibutton>Έναρξης παιχνιδιού</guibutton></term>
          <listitem>
            <para>Το πλήκτρο <guibutton>Έναρξη παιχνιδιού</guibutton> αρχίζει το επισημασμένο παιχνίδι στον κατάλογο παιχνιδιών.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Πλήκτρο <guibutton>Ιδιότητες</guibutton></term>
          <listitem>
            <para>Το πλήκτρο <guibutton>Ιδιότητες</guibutton> εμφανίζει πρόσθετες πληροφορίες για το επισημασμένο παιχνίδι στον κατάλογο παιχνιδιών.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Γραμμή κατάστασης</term>
          <listitem>
            <para>Η γραμμή κατάστασης εμφανίζει την έκδοση <application>MAME</application> που χρησιμοποιούταν για να παίξει παιχνίδια και να εμφανίσει πληροφορίες για τα στοιχεία μενού. Εμφανίζει επίσης μια γραμμή προόδου, ενώ δομεί τη βάση δεδομένων του παιχνιδιού.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
  </section>

  <section>
    <title>Χρήση</title>
    <section>
      <title>Για να αρχίσετε ένα παιχνίδι</title>
      <para>Επιλέξτε ένα παιχνίδι από τον κατάλογο παιχνιδιών και πατήστε πάνω του για να το επισημάνετε. Έπειτα πατήστε το πλήκτρο <guibutton>Έναρξη παιχνιδιού</guibutton> ή επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap> <keycap>S</keycap> </keycombo> </shortcut> <guimenu>Παιχνίδι</guimenu><guimenuitem>Έναρξη</guimenuitem> </menuchoice>.</para>
      <tip>
        <para>Μπορείτε επίσης να αρχίσετε ένα παιχνίδι διπλοπατώντας το στον κατάλογο παιχνιδιών.</para>
      </tip>
    </section>
    <section>
      <title>Για να προβάλετε τις ιδιότητες ενός παιχνιδιού</title>
      <para>Επιλέξτε ένα παιχνίδι από τον κατάλογο παιχνιδιών και πατήστε πάνω του για να το επισημάνετε. Έπειτα πατήστε το πλήκτρο <guibutton>Ιδιότητες</guibutton> ή επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap> <keycap>P</keycap> </keycombo> </shortcut> <guimenu>Προβολή</guimenu><guimenuitem>Ιδιότητες</guimenuitem> </menuchoice>.</para>
      <para>Ο διάλογος <guilabel>Ιδιότητες</guilabel> εμφανίζει πληροφορίες για το επισημασμένο παιχνίδι, συμπεριλαμβάνοντας κάποιο ιστορικό για το παιχνίδι, συμβουλές και κόλπα για να σας βοηθήσουν να βελτιώσετε τις επιδόσεις σας, εικόνες της αρχειοθήκης arcade, στιγμιότυπα του παιχνιδιού σε εξέλιξη και κάποιες τεχνικές πληροφορίες για την προσομοίωση του παιχνιδιού του <application>MAME</application>.</para>
      <para>Για να κλείσετε τον διάλογο <guilabel>Ιδιότητες</guilabel>, πατήστε το πλήκτρο <guibutton>Κλείσιμο</guibutton>.</para>
      <note>
        <para>Κάποια από αυτά τα γνωρίσματα δεν είναι ακόμα ολοκληρωμένα και θα είναι διαθέσιμα σε μια μελλοντική έκδοση του <application>GNOME Βίντεο Arcade</application>.</para>
      </note>
    </section>
    <section>
      <title>Για να ονομάσετε ένα παιχνίδι ως αγαπημένο</title>
      <para>Για να ονομάσετε ένα παιχνίδι ως αγαπημένο, επιλέξτε το παιχνίδι από τον κατάλογο παιχνιδιών και πατήστε το για να το επισημάνετε. Έπειτα επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap> <keycap>+</keycap> </keycombo> </shortcut> <guimenu>Παιχνίδι</guimenu> <guimenuitem>Προσθήκη στα αγαπημένα</guimenuitem> </menuchoice>. Το εικονίδιο καρδιάς για το επισημασμένο παιχνίδι θα φωτίσει για να δείξει ότι το παιχνίδι έχει προστεθεί στον κατάλογο των αγαπημένων σας.</para>
      <para>Αν ένα παιχνίδι δεν ανήκει πια στα αγαπημένα σας, πατήστε το όνομά του στον κατάλογο παιχνιδιών για να το επισημάνετε. Έπειτα επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap> <keycap>-</keycap> </keycombo> </shortcut> <guimenu>Παιχνίδι</guimenu> <guimenuitem>Αφαίρεση από τα αγαπημένα</guimenuitem> </menuchoice>. Το εικονίδιο της καρδιάς για το επισημασμένο παιχνίδι θα γίνει γκρίζο για να δείξει ότι το παιχνίδι έχει αφαιρεθεί από τον κατάλογο των αγαπημένων σας.</para>
      <tip>
        <para>Μπορείτε επίσης να πατήσετε άμεσα στο εικονίδιο της καρδιάς στον κατάλογο παιχνιδιών για να ονομάσετε το παιχνίδι ως αγαπημένο ή όχι.</para>
      </tip>
    </section>
    <section>
      <title>Για να αλλάξετε ποια παιχνίδια εμφανίζονται</title>
      <para>Για να εμφανίσετε όλα τα διαθέσιμα παιχνίδια στον κατάλογο παιχνιδιών, πατήστε το πλήκτρο προβολής <guibutton>Διαθέσιμα παιχνίδια</guibutton> ή επιλέξτε <menuchoice> <guimenu>Προβολή</guimenu> <guimenuitem>Διαθέσιμα παιχνίδια</guimenuitem> </menuchoice>.</para>
      <para>Για να εμφανίσετε μόνο τα αγαπημένα σας παιχνίδια στον κατάλογο παιχνιδιών, πατήστε το πλήκτρο προβολής <guibutton>Αγαπημένα παιχνίδια</guibutton> ή επιλέξτε <menuchoice> <guimenu>Προβολή</guimenu> <guimenuitem>Αγαπημένα παιχνίδια</guimenuitem> </menuchoice>.</para>
      <para>Για να εμφανίσετε τα προηγούμενα αποτελέσματα αναζήτησης στον κατάλογο παιχνιδιών, πατήστε το πλήκτρο προβολής <guibutton>Αποτελέσματα αναζήτησης</guibutton> ή επιλέξτε <menuchoice> <guimenu>Προβολή</guimenu> <guimenuitem>Αποτελέσματα αναζήτησης</guimenuitem> </menuchoice>.</para>
    </section>
    <section>
      <title>Για να αναζητήσετε παιχνίδια</title>
      <para>Επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap><keycap>F</keycap> </keycombo> </shortcut> <guimenu>Επεξεργασία</guimenu><guimenuitem>Αναζήτηση...</guimenuitem> </menuchoice> για να εμφανίσετε τον παρακάτω διάλογο:</para>
      <figure>
        <title>Ο διάλογος αναζήτησης του GNOME Βίντεο Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_search_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Εμφανίζει τον διάλογο αναζήτησης του GNOME Βίντεο Arcade. Περιέχει το πλαίσιο καταχώριση κειμένου, το πλήκτρο εύρεσης και το πλήκτρο κλεισίματος.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Πληκτρολογήστε τα κριτήρια αναζήτησης στο πλαίσιο καταχώρισης. Αναζητήστε κατά τον τίτλο του παιχνιδιού, του κατασκευαστή του παιχνιδιού, του έτους έκδοσης του παιχνιδιού ή του ονόματος του συνόλου ROM του παιχνιδιού ή του οδηγού του <application>MAME</application>.</para>
      <para>Πατήστε το πλήκτρο <guibutton>Εύρεση</guibutton> για να αρχίσετε την αναζήτησή σας. Ο διάλογος <guilabel>Αναζήτηση</guilabel> θα κλείσει και τα αποτελέσματα της αναζήτησής σας θα εμφανιστούν στον κατάλογο παιχνιδιών.</para>
      <para>Πατήστε το πλήκτρο <guibutton>Κλείσιμο</guibutton> για να κλείσετε τον διάλογο <guilabel>Αναζήτηση</guilabel> χωρίς να ψάξετε.</para>
    </section>
    <section>
      <title>Για να γράψετε ένα παιχνίδι</title>
      <para>Επιλέξτε ένα παιχνίδι από τον κατάλογο παιχνιδιών και πατήστε πάνω του για να το επισημάνετε. Έπειτα επιλέξτε <menuchoice> <shortcut> <keycombo> <keycap>Ctrl</keycap> <keycap>R</keycap> </keycombo> </shortcut> <guimenu>Παιχνίδι</guimenu><guimenuitem>Εγγραφή</guimenuitem> </menuchoice>.</para>
      <para>Αφού βγείτε από το <application>MAME</application>, η νέα σας εγγραφή θα επισημανθεί στο παρακάτω παράθυρο:</para>
      <figure>
        <title>Παράθυρο γραμμένων παιχνιδιών του GNOME Βίντεο Arcade</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_recorded_games.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Εμφανίζει το παράθυρο γραμμένων παιχνιδιών του GNOME Βίντεο Arcade. Περιέχει τον κατάλογο των γραμμένων παιχνιδιών, το πλήκτρο αναπαραγωγής, το πλήκτρο διαγραφής και το πλήκτρο κλεισίματος.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Κάθε καταχώριση εμφανίζει τον τίτλο του παιχνιδιού που γράφτηκε και την ημερομηνία και τον χρόνο που άρχισε η εγγραφή. Πατήστε στον τίτλο του παιχνιδιού για να προσαρμόσετε το σχόλιο. Μπορείτε να προσθέσετε περισσότερες πληροφορίες στο σχόλιο όπως το τελικό αποτέλεσμα ή το επίπεδο που φτάσατε.</para>
    </section>
    <section>
      <title>Για να αναπαράξετε ένα γραμμένο παιχνίδι</title>
      <para>Επιλέξτε <menuchoice> <guimenu>Παιχνίδι</guimenu> <guimenuitem>Αναπαραγωγή...</guimenuitem> </menuchoice> για να εμφανίσετε το παράθυρο <guilabel>Γραμμένα παιχνίδια</guilabel>. Επιλέξτε μια εγγραφή παιχνιδιού για αναπαραγωγή και πατήστε την για να την επισημάνετε. Έπειτα πατήστε το πλήκτρο <guibutton>Αναπαραγωγή</guibutton>.</para>
    </section>
  </section>

  <section>
    <title>Προτιμήσεις</title>
    <para>Για να τροποποιήσετε τις προτιμήσεις του <application>GNOME Βίντεο Arcade</application>, επιλέξτε <menuchoice> <guimenu>Επεξεργασία</guimenu><guimenuitem>Προτιμήσεις</guimenuitem> </menuchoice>.</para>
    <para>Ο διάλογος <guilabel>Προτιμήσεις</guilabel> περιέχει τις ακόλουθες ενότητες:</para>
    <section>
      <title>Γενικά</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_general_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Εμφανίζει την καρτέλα γενικά του παραθύρου προτιμήσεις.</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <variablelist>
        <varlistentry>
          <term><guilabel> Έναρξη παιχνιδιών σε κατάσταση πλήρους οθόνης </guilabel></term>
          <listitem>
            <para>Επιλέξτε αυτήν την επιλογή για να αρχίσετε τα παιχνίδια σε κατάσταση πλήρους οθόνης. Αλλιώς, τα παιχνίδια θα αρχίσουν σε ένα παράθυρο.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><guilabel> Επαναφορά της προηγούμενης κατάστασης κατά την έναρξη ενός παιχνιδιού </guilabel></term>
          <listitem>
            <para>Επιλέξτε αυτήν την επιλογή για να συνεχίσετε όπου αφήσατε, όταν βγήκατε τελευταία από το <application>MAME</application>. Αν αυτή η επιλογή δεν επιλεγεί, ο προσομοιωτής θα εκκινεί τη μηχανή arcade κάθε φορά που ένα παιχνίδι ξεκινά.</para>
            <note>
              <para>Η επαναφορά κατάστασης δεν εφαρμόζεται μέχρι να ξεκινήσει για δεύτερη φορά το παιχνίδι με ενεργή την επιλογή. Δεν εφαρμόζεται στην εγγραφή ή αναπαραγωγή παιχνιδιών.</para>
            </note>
            <warning>
              <para>Δεν υποστηρίζουν όλα τα παιχνίδια αυτό το γνώρισμα.</para>
            </warning>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><guilabel> Εμφάνιση εναλλακτικών εκδόσεων των αρχικών παιχνιδιών </guilabel></term>
          <listitem>
            <para>Επιλέξτε αυτήν την επιλογή για να εμφανίσετε εναλλακτικές εκδόσεις των αρχικών παιχνιδιών στον κατάλογο παιχνιδιών στο κυρίως παράθυρο.</para>
          </listitem>
        </varlistentry>
      </variablelist>
      <note>
        <title>Ποιες είναι οι εναλλακτικές εκδόσεις;</title>
          <para>Από το <ulink url="http://mamedev.org/devwiki/index.php/FAQ:General_Games"><application>MAME</application> Συχνές ερωτήσεις</ulink>:</para>
          <blockquote>
            <para>Τα παιχνίδια αδειοδοτούνται συχνά σε άλλες εταιρείες για διάφορους λόγους: ο πιο συνηθισμένος είναι ότι οι δημιουργοί επιθυμούν να πουλήσουν το παιχνίδι σε μια χώρα ή περιοχή στην οποία δεν έχουν δικιά τους διανομή. Στις αρχές 80, η Namco δεν είχε δίκτυο διανομής στις ΗΠΑ, έτσι τα παιχνίδια της αδειοδοτήθηκαν στους Atari, Bally-Midway και σε άλλους. Αντίστροφα, η Atari αδειοδότησε τα παιχνίδια της σε ιαπωνικές εταιρείες (συνήθως τη Namco) στην Ιαπωνία.</para>
            <para>Μερικές φορές ένα σύνολο ROMs με διαφορετική ημερομηνία πνευματικών δικαιωμάτων βρίσκεται, ή ένα λαθραίο σύνολο, ή άλλη εναλλακτική έκδοση. Όταν το <quote>γονικό</quote> παιχνίδι έχει ήδη προσομοιωθεί στο <application>MAME</application>, αυτοί οι αντικαταστάτες είναι συνήθως εύκολο να προστεθούν. Σε κάποιες περιπτώσεις, οι εναλλακτικές εκδόσεις είναι ελαφρά διαφορετικές: Τα επίπεδα του παιχνιδιού είναι με διαφορετική σειρά, το παιχνίδι είναι πιο δύσκολο ή πιο γρήγορο, κλπ.</para>
            <para>Σε κάποιες περιπτώσεις, υπήρξαν επεμβάσεις στα ROMs για να τρέξουν σε υλικό για το οποίο δεν προοριζόντουσαν. Για παράδειγμα, όταν το <productname>Pac-Man</productname> ήταν <quote>πρόσφατο,</quote> κάποιοι χειριστές του arcade βρήκαν έναν τρόπο να μεταφέρουν τα ROMs του <productname>Pac-Man</productname> και να επέμβουν σε αυτά για να δουλέψουν σε μηχανές <productname>Scramble</productname>, έτσι δεν έπρεπε να αγοράσουν πρόσθετες πλακέτες του <productname>Pac-Man</productname>. Από τότε που αυτές οι εκδόσεις με επεμβάσεις έγιναν θαυμάσιες με τα δικά τους δικαιώματα, οι προγραμματιστές του <application>MAME</application> τις συμπεριέλαβαν.</para>
          </blockquote>
      </note>
    </section>
    <section>
      <title>Στήλες</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_column_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Εμφανίζει την καρτέλα με τις στήλες στο παράθυρο προτιμήσεων.</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <para>Η καρτέλα <guilabel>Στήλες</guilabel> σας επιτρέπει να ρυθμίσετε τον κατάλογο παιχνιδιών στο κυρίως παράθυρο για να εμφανίσετε πρόσθετες πληροφορίες για τα διαθέσιμα παιχνίδια. Τα ονόματα στηλών καταχωρίζονται με τη σειρά που εμφανίζονται στον κατάλογο παιχνιδιών. Αναδιατάξτε τις στήλες και εμφανίστε πρόσθετες στήλες χρησιμοποιώντας τα πλήκτρα <guibutton>Πάνω</guibutton>, <guibutton>Κάτω</guibutton>, <guibutton>Εμφάνιση</guibutton> και <guibutton>Απόκρυψη</guibutton> στα δεξιά του καταλόγου στηλών.</para>
    </section>
  </section>

  <section>
    <title>Αντιμετώπιση προβλημάτων</title>
    <anchor id="troubleshooting"/>
    <section>
      <title>Δεν υπάρχουν καταχωρισμένα παιχνίδια στο κυρίως παράθυρο</title>
        <section>
          <title>Είναι εγκατεστημένες εικόνες ROM;</title>
          <para>Για νομικούς λόγους, τα παιχνίδια δεν είναι πακεταρισμένα με το <application>GNOME Βίντεο Arcade</application> ή <application>MAME</application>. Το <ulink url="http://mamedev.org/devwiki/index.php/FAQ:Setting_Up"><application>MAME</application> Συχνές ερωτήσεις</ulink> έχει κάποιες προτάσεις για τη λήψη εικόνων ROM:</para>
          <blockquote>
            <para>Υπάρχουν πολλά μέσα λήψης εικόνων ROM για χρήση στο <application>MAME</application>:</para>
            <itemizedlist>
              <listitem>
                <para>Ένας μικρός αριθμός εικόνων ROM είναι διαθέσιμες τώρα με την άδεια των κατόχων των πνευματικών δικαιωμάτων στη <ulink url="http://mamedev.org/roms/">σελίδα ROMs του mamedev.org</ulink>.</para>
              </listitem>
              <listitem>
                <para>Ο πίνακας ελέγχου του <ulink url="http://www.hanaho.com/Products/HotRodJoystick.php"><productname>HotRod Joystick</productname></ulink> από τα <ulink url="http://www.hanaho.com/">Hanaho Games, Inc.</ulink> έρχεται πακεταρισμένο με ένα CD που περιλαμβάνει το <application>MAME</application> και μια επιλογή των Capcom ROMs. Το Hanaho επίσης πουλά την αρχειοθήκη <ulink url="http://www.hanaho.com/Products/ArcadePC.php"><productname>ArcadePC</productname></ulink> με μια διαφορετική επιλογή των Capcom ROMs.</para>
              </listitem>
              <listitem>
                <para>Μπορείτε να αγοράσετε τα δικά σας PCBs arcade (στο <ulink url="http://www.ebay.com/">eBay</ulink>, για παράδειγμα), να αγοράσετε έναν αναγνώστη ROM και να τον χρησιμοποιήσετε για να αντιγράψετε τα περιεχόμενα των ολοκληρωμένων ROM σε αρχεία για προσωπική χρήση.</para>
              </listitem>
            </itemizedlist>
            <para>Να ελέγχεται πάντα αν ο κάτοχος πνευματικών δικαιωμάτων των συγκεκριμένων παιχνιδιών arcade πουλά τα ROMs νόμιμα (όπως στην περίπτωση Capcom). Έτσι θα υποστηρίξετε τις εταιρείες που υποστηρίζουν προσομοίωση.</para>
          </blockquote>
        </section>
        <section>
          <title>Μπορεί το <application>MAME</application> να βρει τις εγκατεστημένες εικόνες ROM;</title>
          <para>Το <application>GNOME Βίντεο Arcade</application> βασίζεται στο <application>MAME</application> για να βρει τις εγκατεστημένες εικόνες ROM. Για να ελέγξετε πού το <application>MAME</application> είναι ρυθμισμένο να αναζητήσει για εικόνες ROM, πληκτρολογήστε την παρακάτω εντολή σε μια γραμμή εντολών, έπειτα πατήστε το πλήκτρο <keycap>επιστροφή</keycap>:</para>
          <para><command>gnome-video-arcade --inspect rompath</command></para>
          <para>Η εντολή πρέπει να εμφανίσει ένα όνομα καταλόγου όπως <computeroutput>/usr/share/mame/roms</computeroutput>. Μετακινήστε τις εικόνες ROM σε αυτόν τον κατάλογο και επανεκκινήστε το <application>GNOME Βίντεο Arcade</application>.</para>
          <para>Αν η εντολή δεν εμφανίζει ένα όνομα καταλόγου, μπορεί να σημαίνει ότι η <application>MAME</application> δεν είναι σωστά ρυθμισμένη στο σύστημά σας. Αν πήρατε το <application>MAME</application> από μια διανομή <productname>GNU/Λίνουξ</productname>, παρακαλούμε να επικοινωνήσετε με τους συντηρητές αυτής της διανομής για παραπέρα οδηγίες.</para>
        </section>
        <section>
          <title>Είναι επιλεγμένη η προβολή διαθέσιμα παιχνίδια;</title>
          <para>Αν δεν έχουν χαρακτηριστεί παιχνίδια ως αγαπημένα, η προβολή <guilabel>Αγαπημένα παιχνίδια</guilabel> θα είναι κενή. Παρόμοια, αν δεν έχουν εκτελεστεί αναζητήσεις, η προβολή <guilabel>Αποτελέσματα αναζήτησης</guilabel> θα είναι κενή. Μεταβείτε στην προβολή <guilabel>Διαθέσιμα παιχνίδια</guilabel> για να εξασφαλίσετε όλα τα διαθέσιμα παιχνίδια που είναι καταχωρισμένα.</para>
        </section>
    </section>
  </section>

</article>
