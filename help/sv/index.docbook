<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.5.6">
<!ENTITY manrevision "1.1">
<!ENTITY date "February 2008">
<!ENTITY app "GNOME Video Arcade">
]>
<article id="index" lang="sv">

  <articleinfo>

    <title>Handbok för <application>GNOME Arkadspel</application></title>

    <abstract role="description">
      <para>GNOME Arkadspel är en grafisk klient för <application>Multiple Arcade Machine Emulator (MAME)</application>.</para>
    </abstract>

    <copyright><year>2007</year> <year>2008</year> <holder>Matthew Barnes</holder></copyright>

      <legalnotice id="legalnotice">
	<para>Tillstånd att kopiera, distribuera och/eller modifiera detta dokument ges under villkoren i GNU Free Documentation License (GFDL), version 1.1 eller senare, utgivet av Free Software Foundation utan standardavsnitt och omslagstexter.  En kopia av GFDL finns att hämta på denna <ulink type="help" url="help:fdl">länk</ulink> eller i filen COPYING-DOCS som medföljer denna handbok.</para>
         <para>Denna handbok utgör en av flera GNOME-handböcker som distribueras under villkoren i GFDL.  Om du vill distribuera denna handbok separat från övriga handböcker kan du göra detta genom att lägga till en kopia av licensavtalet i handboken enligt instruktionerna i avsnitt 6 i licensavtalet.</para>

	<para>Flera namn på produkter och tjänster är registrerade varumärken. I de fall dessa namn förekommer i GNOME-dokumentation - och medlemmarna i GNOME-dokumentationsprojektet är medvetna om dessa varumärken - är de skrivna med versaler eller med inledande versal.</para>

	<para>DOKUMENTET OCH MODIFIERADE VERSIONER AV DOKUMENTET TILLHANDAHÅLLS UNDER VILLKOREN I GNU FREE DOCUMENTATION LICENSE ENDAST UNDER FÖLJANDE FÖRUTSÄTTNINGAR: <orderedlist>
		<listitem>
		  <para>DOKUMENTET TILLHANDAHÅLLS I "BEFINTLIGT SKICK" UTAN NÅGRA SOM HELST GARANTIER, VARE SIG UTTRYCKLIGA ELLER UNDERFÖRSTÅDDA, INKLUSIVE, MEN INTE BEGRÄNSAT TILL, GARANTIER ATT DOKUMENTET ELLER EN MODIFIERAD VERSION AV DOKUMENTET INTE INNEHÅLLER NÅGRA FELAKTIGHETER, ÄR LÄMPLIGT FÖR ETT VISST ÄNDAMÅL ELLER INTE STRIDER MOT LAG. HELA RISKEN VAD GÄLLER KVALITET, EXAKTHET OCH UTFÖRANDE AV DOKUMENTET OCH MODIFIERADE VERSIONER AV DOKUMENTET LIGGER HELT OCH HÅLLET PÅ ANVÄNDAREN. OM ETT DOKUMENT ELLER EN MODIFIERAD VERSION AV ETT DOKUMENT SKULLE VISA SIG INNEHÅLLA FELAKTIGHETER I NÅGOT HÄNSEENDE ÄR DET DU (INTE DEN URSPRUNGLIGA SKRIBENTEN, FÖRFATTAREN ELLER NÅGON ANNAN MEDARBETARE) SOM FÅR STÅ FÖR ALLA EVENTUELLA KOSTNADER FÖR SERVICE, REPARATIONER ELLER KORRIGERINGAR. DENNA GARANTIFRISKRIVNING UTGÖR EN VÄSENTLIG DEL AV DETTA LICENSAVTAL. DETTA INNEBÄR ATT ALL ANVÄNDNING AV ETT DOKUMENT ELLER EN MODIFIERAD VERSION AV ETT DOKUMENT BEVILJAS ENDAST UNDER DENNA ANSVARSFRISKRIVNING;</para>
		</listitem>
		<listitem>
		  <para>UNDER INGA OMSTÄNDIGHETER ELLER INOM RAMEN FÖR NÅGON LAGSTIFTNING, OAVSETT OM DET GÄLLER KRÄNKNING (INKLUSIVE VÅRDSLÖSHET), KONTRAKT ELLER DYLIKT, SKA FÖRFATTAREN, DEN URSPRUNGLIGA SKRIBENTEN ELLER ANNAN MEDARBETARE ELLER ÅTERFÖRSÄLJARE AV DOKUMENTET ELLER AV EN MODIFIERAD VERSION AV DOKUMENTET ELLER NÅGON LEVERANTÖR TILL NÅGON AV NÄMNDA PARTER STÄLLAS ANSVARIG GENTEMOT NÅGON FÖR NÅGRA DIREKTA, INDIREKTA, SÄRSKILDA ELLER OFÖRUTSEDDA SKADOR ELLER FÖLJDSKADOR AV NÅGOT SLAG, INKLUSIVE, MEN INTE BEGRÄNSAT TILL, SKADOR BETRÄFFANDE FÖRLORAD GOODWILL, HINDER I ARBETET, DATORHAVERI ELLER NÅGRA ANDRA TÄNKBARA SKADOR ELLER FÖRLUSTER SOM KAN UPPKOMMA PÅ GRUND AV ELLER RELATERAT TILL ANVÄNDNINGEN AV DOKUMENTET ELLER MODIFIERADE VERSIONER AV DOKUMENTET, ÄVEN OM PART SKA HA BLIVIT INFORMERAD OM MÖJLIGHETEN TILL SÅDANA SKADOR.</para>
		</listitem>
	  </orderedlist></para>
  </legalnotice>



    <authorgroup>
      <author role="maintainer"><firstname>Matthew</firstname> <surname>Barnes</surname></author>
    </authorgroup>

    <revhistory>
      <revision><revnumber>Handbok för GNOME Arkadspel 1.1</revnumber> <date>2008-02-24</date></revision>
    </revhistory>

    <releaseinfo>Denna handbok beskriver version 0.5.6 av GNOME Arkadspel</releaseinfo>

  
    <othercredit class="translator">
      <personname>
        <firstname>Daniel Nylander</firstname>
      </personname>
      <email>po@danielnylander.se</email>
    </othercredit>
    <copyright>
      
        <year>2009</year>
      
      <holder>Daniel Nylander</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Josef Andersson</firstname>
      </personname>
      <email>josef.andersson@fripost.org</email>
    </othercredit>
    <copyright>
      
        <year>2014</year>
      
      <holder>Josef Andersson</holder>
    </copyright>
  </articleinfo>

  <section>
    <title>Introduktion</title>
    <para>Programmet <application>GNOME Arkadspel</application> låter dig spela klassiska myntdrivna arkadspel på ditt GNOME-skrivbord med hjälp av <application>Multiple Arcade Machine Emulator (MAME)</application>.</para>
    <para><application>GNOME Arkadspel</application> tillhandahåller följande funktioner:</para>
    <itemizedlist>
      <listitem>
        <para>Spela klassiska arkadspel på ditt skrivbord.</para>
      </listitem>
      <listitem>
        <para>Markera dina favoritspel för att enkelt hitta dem senare.</para>
      </listitem>
      <listitem>
        <para>Läs om historik och tips för dina favoritspel.</para>
      </listitem>
      <listitem>
        <para>Spela in spelet och spela upp.</para>
      </listitem>
      <listitem>
        <para>Sök.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section>
    <title>Komma igång</title>
    <section>
      <title>Starta GNOME Arkadspel</title>
      <para>Du kan starta <application>GNOME Arkadspel</application> på följande sätt:</para>
      <variablelist>
        <varlistentry>
          <term><guimenu>Program</guimenu>-menyn</term>
          <listitem>
            <para>Välj <menuchoice><guisubmenu>Spel</guisubmenu><guimenuitem>Arkadspel</guimenuitem></menuchoice>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Kommandorad</term>
          <listitem>
            <para>För att starta <application>GNOME Arkadspel</application> från en kommandorad, skriv in följande kommando och tryck sedan <keycap>Retur</keycap>:</para>
            <para><command>gnome-video-arcade</command></para>
            <tip>
              <para>För att se andra tillgängliga kommandoradsalternativ, skriv <command>gnome-video-arcade --help</command> och tryck sedan <keycap>Retur</keycap>.</para>
            </tip>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
    <section>
      <title>När du startar GNOME Arkadspel</title>
      <para>Första gången <application>GNOME Arkadspel</application> startar bygger den upp en databas av spelinformation och analyserar tillgängliga ROM-filer för att avgöra vilka spel som är spelbara. Denna process kan ta flera minuter att genomföra. Under tiden kommer följande fönster att visas:</para>
      <figure>
        <title>GNOME Arkadspel bygger speldatabasen</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_building_database.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Visar huvudfönstret för GNOME Arkadspel medan databasen byggs. De flesta element är utgråade, och en förloppsindikator visas längst ned.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Fönstret kommer sedan att ändras till detta:</para>
      <figure>
        <title>Startfönster för GNOME Arkadspel</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_main_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Visar huvudfönstret för GNOME Arkadspel med en urvalslista av spel. Innehåller menyrad, spellista, vyknappar, spelstartknapp, egenskapsknapp och en statusrad.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Du kanske ser en annan spellista beroende på vilka ROM-avbilder som är installerade på ditt system.</para>
      <tip>
        <para>Om du vet om att ROM-avbilder är installerade på ditt system men <application>GNOME Arkadspel</application> inte visar några spel kan det bero på att <application>MAME</application> är felkonfigurerad eller att inga av de installerade ROM-avbilderna är spelbara. Se problemlösningsavsnittet för mer information.</para>
      </tip>
      <note>
        <para>Ibland kan databasen byggas om när <application>GNOME Arkadspel</application> startar. För det mesta hoppar den över denna process och går direkt till fönstret i figur 2.</para>
      </note>
      <para>Fönstret för <application>GNOME Arkadspel</application> innehåller följande element:</para>
      <variablelist>
        <varlistentry>
          <term>Menyrad</term>
          <listitem>
            <para>Menyerna på menyraden innehåller alla kommandon som behövs för att använda <application>GNOME Arkadspel</application>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Spellista</term>
          <listitem>
            <para>Spellistan visar en lista över spel att välja från.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Vyknappar</term>
          <listitem>
            <para>Vyknapparna kontrollerar vilka spel som visas i spellistan. Välj mellan att visa alla tillgängliga spel, en lista av favoritspel eller tidigare sökresultat.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Knappen <guibutton>Starta spel</guibutton></term>
          <listitem>
            <para>Knappen <guibutton>Starta spel</guibutton> startar det markerade spelet i spellistan.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Knappen <guibutton>Egenskaper</guibutton></term>
          <listitem>
            <para>Knappen <guibutton>Egenskaper</guibutton> visar ytterligare information om det markerade spelet i spellistan.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Statusrad</term>
          <listitem>
            <para>Statusraden visar <application>MAME</application>-versionen som används för att spela spel och annan information om menyobjekten. Den visar också en förloppsindikator under tiden speldatabasen byggs.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </section>
  </section>

  <section>
    <title>Användning</title>
    <section>
      <title>Starta ett spel</title>
      <para>Välj ett spel från spellistan och klicka på det för att markera det. Klicka sedan på knappen <guibutton>Starta spel</guibutton> eller välj <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap>S</keycap></keycombo></shortcut><guimenu>Spel</guimenu><guimenuitem>Starta</guimenuitem></menuchoice>.</para>
      <tip>
        <para>Du kan också starta ett spel genom att dubbelklicka på det i spellistan.</para>
      </tip>
    </section>
    <section>
      <title>För att visa egenskaper för ett spel</title>
      <para>Välj ett spel från spellistan och klicka på det för att markera det. Välj sedan knappen <guibutton>Egenskaper</guibutton> eller välj <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap>P</keycap></keycombo></shortcut><guimenu>Visa</guimenu><guimenuitem>Egenskaper</guimenuitem></menuchoice>.</para>
      <para>Dialogen <guilabel>Egenskaper</guilabel> visar information om det markerade spelet, inklusive en del historik om spelet, tips och trick för att hjälpa dig höja poängen, bilder av arkadkabinettet, skärmdumpar från spelet, och en del teknisk information om <application>MAME</application>-emuleringen av spelet.</para>
      <para>För att stänga dialogen <guilabel>Egenskaper</guilabel>,klicka på knappen <guibutton>Stäng</guibutton>.</para>
      <note>
        <para>En del av dessa egenskaper är ännu inte färdiga och kommer att bli tillgängliga i en framtida version av <application>GNOME Arkadspel</application>.</para>
      </note>
    </section>
    <section>
      <title>Markera ett spel som favoritspel</title>
      <para>För att markera spelet som en favorit, välj ett spel från spellistan och klicka på det för att markera det. Välj sedan <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap>+</keycap></keycombo></shortcut><guimenu>Spel</guimenu><guimenuitem>Lägg till i favoriter</guimenuitem></menuchoice>. Hjärtikonen för det markerade spelet kommer att lysa upp för att visa att spelet har lagts till i listan över favoriter.</para>
      <para>Om ett spel inte längre är din favorit, klicka på dess namn i spellistan för att markera det. Välj sedan <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap/></keycombo></shortcut><guimenu>Spel</guimenu><guimenuitem>Ta bort från favoriter</guimenuitem></menuchoice>. Hjärtikonen för det markerade spelet kommer att bli grå för att visa att spelet har tagits bort från din lista över favoriter.</para>
      <tip>
        <para>Du kan även klicka direkt på hjärtikonen i spellistan för att markera spelet som ett favoritspel (eller inte).</para>
      </tip>
    </section>
    <section>
      <title>För att ändra vilka spel som visas</title>
      <para>För att visa alla tillgängliga spel i spellistan, klicka på visningsknappen <guibutton>Tillgängliga spel</guibutton> eller välj <menuchoice><guimenu>Visa</guimenu><guimenuitem>Tillgängliga spel</guimenuitem> </menuchoice>.</para>
      <para>För att endast visa dina favoritspel i spellistan, klicka på vyknappen <guibutton>Favoritspel</guibutton> eller välj <menuchoice><guimenu>Visa</guimenu><guimenuitem>Favoritspel</guimenuitem></menuchoice></para>
      <para>För att visa tidigare sökresultat i spellistan, klicka vyknappen <guibutton>Sökresultat</guibutton> eller välj <menuchoice><guimenu>Visa</guimenu><guimenuitem>Sökresultat</guimenuitem></menuchoice>.</para>
    </section>
    <section>
      <title>Sök efter spel</title>
      <para>Välj <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap>F</keycap></keycombo></shortcut><guimenu>Redigera</guimenu><guimenuitem>Sök…</guimenuitem></menuchoice> för att visa följande dialogruta:</para>
      <figure>
        <title>Sökruta för GNOME Arkadspel</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_search_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Visar sökdialogen för GNOME Arkadspel. Innehåller fält för textinmatning, sökknapp och en stängknapp.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Skriv in din sökterm i sökfältet. Sök efter speltitel, tillverkaren som distribuerade spelet, året spelet släpptes, spelets ROM-namn eller drivrutin för <application>MAME</application>.</para>
      <para>Klicka på knappen <guibutton>Sök</guibutton> för att starta din sök. Dialogen <guilabel>Sök</guilabel> kommer att stängas och resultaten av din sökning kommer att visas i spellistan.</para>
      <para>Klicka på knappen <guibutton>Stäng</guibutton> för att stänga <guilabel>Sök</guilabel>-dialogen utan att söka.</para>
    </section>
    <section>
      <title>Spela in ett spel</title>
      <para>Välj ett spel från spellistan och klicka på det för att markera det. Välj sedan <menuchoice><shortcut><keycombo><keycap>Ctrl</keycap><keycap>R</keycap></keycombo></shortcut><guimenu>Spel</guimenu><guimenuitem>Spela in</guimenuitem></menuchoice>.</para>
      <para>Efter att du avslutat <application>MAME</application> kommer din nya inspelning att markeras i följande fönster:</para>
      <figure>
        <title>GNOME Arkadspels fönster för inspelade spel</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/gva_recorded_games.png" format="PNG"/>
            </imageobject>
            <textobject>
              <para>Visar GNOME Arkadspels fönster för inspelade spel. Innehåller lista över spelinspelningar, uppspelningsknapp, ta bort-knapp och stängknapp.</para>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
      <para>Var post visar titeln på spelet som spelades in samt datum och tid för när inspelningen påbörjades. Klicka på spelets titel för att anpassa kommentaren. Du kan lägga till mer information till kommentaren, som den slutliga poängen eller nådd nivå.</para>
    </section>
    <section>
      <title>Spela upp ett inspelat spel</title>
      <para>Välj<menuchoice><guimenu>Spel</guimenu><guimenuitem>Spela upp…</guimenuitem></menuchoice> för att visa fönstret <guilabel>Inspelade spel</guilabel>. Välj en spelinspelning att spela upp och klicka på den för att markera en. Välj sedan knappen <guibutton>Spela upp</guibutton>.</para>
    </section>
  </section>

  <section>
    <title>Inställningar</title>
    <para>För att ändra inställningarna för <application>GNOME Arkadspel</application>, välj <menuchoice><guimenu>Redigera</guimenu><guimenuitem>Inställningar</guimenuitem></menuchoice>.</para>
    <para>Dialogen <guilabel>Inställningar</guilabel> innehåller följande sektioner:</para>
    <section>
      <title>Allmänt</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_general_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Visar fliken Allmänt under fönstret egenskaper.</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <variablelist>
        <varlistentry>
          <term><guilabel> Starta spel i helskärmsläge </guilabel></term>
          <listitem>
            <para>Välj detta alternativ för att starta spel i helskärmsläge. Annars kommer spel att startas i ett fönster.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><guilabel> Återställ föregående tillstånd när ett spel startas </guilabel></term>
          <listitem>
            <para>Välj detta alternativ för att fortsätta där du lämnade när du senast avslutade <application>MAME</application>. Om detta alternativ inte är markerat kommer emulatorn att starta upp arkadmaskinen var gång spelet startas.</para>
            <note>
              <para>Tillståndsåterställning verkar inte förrän andra gången spelet startas med alternativet aktiverat. Det gäller inte för inspelning eller uppspelning av spel.</para>
            </note>
            <warning>
              <para>Inte alla spel har stöd för denna funktion.</para>
            </warning>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><guilabel> Visa alternativa versioner av ursprungliga spel </guilabel></term>
          <listitem>
            <para>Välj detta alternativ för att visa alternativa versioner av originalspel i spellistan i huvudfönstret.</para>
          </listitem>
        </varlistentry>
      </variablelist>
      <note>
        <title>Vad är alternativa versioner?</title>
          <para>Från <ulink url="http://mamedev.org/devwiki/index.php/FAQ:General_Games"><application>MAME</application> FAQ</ulink>:</para>
          <blockquote>
            <para>Spel licenserades ofta ut till andra bolag av olika skäl: det vanligaste var att tillverkarna ville sälja spelet i ett land eller område där de inte hade någon egen distribution. På det tidiga 80-talet hade Namco inget distributionsnätverk i USA, och deras spel licenserades till Atari, Bally-Midway och andra. På motsvarande sätt licenserade Atari sina spel till japanska bolag (vanligen Namco) för försäljning.</para>
            <para>Ibland hittas ett ROM-set med ett annan upphovsrättsdatum eller ett bootleg-set, eller en annan alternativ version. När det <quote>överordnade</quote> spelet redan har emulerats i <application>MAME</application> är alternativen vanligtvis lätta att lägga till. I vissa fall är alternativa version något annorlunda. Spelnivåer kommer i en annan ordning, spelet är svårare eller snabbare och så vidare.</para>
            <para>I vissa fall hackades ROM:ar för att köras på hårdvara de inte var skapade för. Till exempel, när <productname>Pac-man</productname> blev <quote>inne</quote> hittade en del arkadoperatörer ett sätt att dumpa <productname>Pac-man</productname> ROM:arna och hacka dem till att fungera på <productname>Scramble</productname>-maskiner så att de inte behövde köpa extra <productname>Pac-man</productname>-moderkort. Eftersom dessa hackade versioner är mästerverk på sitt sätt har <application>MAME</application>-utvecklarna inkluderat dem.</para>
          </blockquote>
      </note>
    </section>
    <section>
      <title>Kolumner</title>
      <screenshot>
        <mediaobject>
          <imageobject>
            <imagedata fileref="figures/gva_column_preferences.png" format="PNG"/>
          </imageobject>
          <textobject>
            <para>Visa kolumnfliken för egenskapsfönstret.</para>
          </textobject>
        </mediaobject>
      </screenshot>
      <para>Fliken <guilabel>Kolumner</guilabel> låter dig konfigurera spellistan i huvudfönstret för att visa ytterligare information om tillgängliga spel. Kolumnnamnen listas i den ordning som de visas i spellistan. Ordna om kolumnerna och visa ytterligare information med knapparna <guibutton>Flytta upp</guibutton>, <guibutton>Flytta ned</guibutton>, <guibutton>Visa</guibutton> och <guibutton>Dölj</guibutton> till höger om kolumnlistan.</para>
    </section>
  </section>

  <section>
    <title>Felsökning</title>
    <anchor id="troubleshooting"/>
    <section>
      <title>Inga spel listas i huvudfönstret</title>
        <section>
          <title>Finns det några ROM-avbildningar installerade?</title>
          <para>Av juridiska skäl följer inga spel med <application>GNOME Arkadspel</application> eller <application>MAME</application>.  <ulink url="http://mamedev.org/devwiki/index.php/FAQ:Setting_Up"><application>MAME</application> FAQ:n</ulink> föreslår några sätt att få tag i ROM-avbilder.</para>
          <blockquote>
            <para>Det finns flera sätt att få tag i ROM-avbilder att använda i <application>MAME</application> på:</para>
            <itemizedlist>
              <listitem>
                <para>Ett mindre antal ROM-avbilder är tillgängliga med upphovsrättsägarens tillstånd på  <ulink url="http://mamedev.org/roms/">ROM-sidan för mamedev.org</ulink>.</para>
              </listitem>
              <listitem>
                <para>Kontrollpanelen <ulink url="http://www.hanaho.com/Products/HotRodJoystick.php"><productname>HotRod Joystick</productname></ulink> från <ulink url="http://www.hanaho.com/">Hanaho Games, Inc.</ulink> inkluderar en CD med <application>MAME</application> och ett urval av  Capcom ROM-avbilder. Hanaho säljer också  <ulink url="http://www.hanaho.com/Products/ArcadePC.php"><productname>ArcadePC</productname>-kabinettet </ulink> med ett annat urval Capcom ROM-avbilder.</para>
              </listitem>
              <listitem>
                <para>Du kan köpa dina egna arkad-PBC:er (exempelvis på <ulink url="http://www.ebay.com/">eBay</ulink>, ), köpa en ROM-läsare och använda den för att kopiera ROM-chippens innehåll till filer för personligt bruk.</para>
              </listitem>
            </itemizedlist>
            <para>Kontrollera alltid om arkadspelets upphovsrättsägare säljer ROM-avbilderna lagligt (som i Capcoms fall). På det sättet stödjer du företag som stödjer emulering.</para>
          </blockquote>
        </section>
        <section>
          <title>Kan <application>MAME</application> hitta de installerade ROM-avbildningarna?</title>
          <para><application>GNOME Arkadspel</application> använder <application>MAME</application> för att hitta installerade ROM-avbilder. För att testa om <application>MAME</application> är konfigurerad att leta efter ROM-avbilder, skriv in följande kommando på kommandoraden och tryck sedan <keycap>Retur</keycap>:</para>
          <para><command>gnome-video-arcade --inspect rom-sökväg</command></para>
          <para>Kommandot borde skriva ut ett katalognamn som exempelvis <computeroutput>/usr/share/mame/roms</computeroutput>. Flytta ROM-avbilder till denna katalog och starta om <application>GNOME Arkadspel</application>.</para>
          <para>Om kommandot inte skriver ut ett katalognamn kan det betyda att <application>MAME</application> inte är rätt konfigurerat på ditt system. Om du erhöll <application>MAME</application> från en <productname>GNU/Linux</productname>-distribution, kontakta ansvariga för den distributionen för vidare instruktioner.</para>
        </section>
        <section>
          <title>Är vyn tillgängliga spel vald?</title>
          <para>Om inga spel har taggats som favoriter kommer <guilabel>Favoritspel</guilabel> att vara tom. På liknande sätt, om inga sökningar ägt rum så kommer vyn <guilabel>Sökresultat</guilabel> att vara tom. Växla till <guilabel>Tillgängliga spel</guilabel> för att vara säker på att alla spel visas.</para>
        </section>
    </section>
  </section>

</article>
